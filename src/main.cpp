/*
* Copyright (c) 2011-2013 by the original author and all contributors
* of this file. All rights reserved.
*
* Author: Feng Zhang (zhjinf@gmail.com)
* Date  : 2013-11-04
* Note  : This work is a part of my research during my Ph.D study at
*         Software Reenginnering Lab of Queen's University at
*         Kingston, Ontario, Canada.
*
* History:
*    (Format: Version, Date, Developer, Comments)
*    1.0, 2013-11-04, Feng Zhang (zhjinf@gmail.com), Add this header.
*/

#include <iostream>

#include <QCoreApplication>
#include <QStringList>

#include "fzkcdb.h"

void parseCLI(const QStringList &arguments)
{
	if(arguments.size()<2) return;
	// foreach(const QString &arg, arguments)
	// {
	// 	std::cout<<qPrintable(arg+"\n");
	// }

	QString command = arguments[1];
	QStringList args = arguments;
	args.removeFirst(); // remove executable itself
	args.removeFirst(); // remove command

	// execute command
	if( 0==command.compare("testKCDB", Qt::CaseInsensitive) )
	{
		FZKCDB db;
		db.open("a");
		db.set("a","b");
		db.get("a");
		db.keys();
		db.close();
	}
}

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

	QStringList arguments;
	for(int i=0; i<argc; i++)
	{
		arguments.append(QString(argv[i]));
	}
	parseCLI(arguments);

	return 0;
}

