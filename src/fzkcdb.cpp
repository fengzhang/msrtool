/*
* Copyright (c) 2011-2013 by the original author and all contributors
* of this file. All rights reserved.
*
* Author: Feng Zhang (zhjinf@gmail.com)
* Date  : 2013-11-04
* Note  : This work is a part of my research during my Ph.D study at
*         Software Reenginnering Lab of Queen's University at
*         Kingston, Ontario, Canada.
*
* History:
*    (Format: Version, Date, Developer, Comments)
*    1.0, 2013-11-04, Feng Zhang (zhjinf@gmail.com), Add this header.
*/

#include "fzkcdb.h"

FZKCDB::FZKCDB()
{
}

FZKCDB::~FZKCDB()
{
}

// open the database
int FZKCDB::open(const QString &databaseFileName)
{
	int rc = 0;
	if(!db.open("casket.kct", TreeDB::OWRITER | TreeDB::OCREATE))
	{
		cerr << "open error: " << db.error().name() << endl;
		rc = -1;
	}

	return rc;
}

// close the database
int FZKCDB::close()
{
	int rc = 0;
	if (!db.close())
	{
		cerr << "close error: " << db.error().name() << endl;
		rc = -1;
	}

	return rc;
}

// store records
int FZKCDB::set(const QString &key, const QByteArray &value)
{
	int rc = 0;
	if (!db.set("foo", "hop") ||
		!db.set("bar", "step") ||
		!db.set("baz", "jump"))
	{
		cerr << "set error: " << db.error().name() << endl;
		rc = -1;
	}

	return rc;
}

// retrieve a record
QByteArray FZKCDB::get(const QString &key)
{
	QByteArray value;

	string val;
	if (db.get("foo", &val))
	{
		cout << val << endl;
	}
	else
	{
		cerr << "get error: " << db.error().name() << endl;
	}

	return value;
}

QStringList FZKCDB::keys()
{
	QStringList dbKeys;

	// traverse records
	DB::Cursor* cur = db.cursor();
	cur->jump();
	string ckey, cvalue;
	while (cur->get(&ckey, &cvalue, true))
	{
		cout << ckey << ":" << cvalue << endl;
	}
	delete cur;

	return dbKeys;
}

