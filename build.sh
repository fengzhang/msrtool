#!/bin/sh
if [ `uname -s` = "Darwin" ]
then
	export PATH=$PATH:/Applications/QtSDK/Desktop/Qt/474/gcc/bin
	qmake -o msrtoolMakeFile msrtool.pro -r -spec macx-g++
	make -f msrtoolMakeFile > /dev/null
elif [ `uname -s` = "Linux" ];
then
	export PATH=/scratch2/fzhang/QtSDK/Desktop/Qt/4.8.1/gcc/bin:$PATH
	qmake -o msrtoolMakeFile msrtool.pro -r -spec linux-g++
	make -f msrtoolMakeFile > /dev/null
elif [ `uname -s` = "MINGW32_NT-6.1" ]
# MinGW: Compiling Zlib
# cd zlib-1.2.7/ 
# mingw32-make -f win32/Makefile.gcc
# cp -iv zlib1.dll /mingw/bin
# cp -iv zconf.h zlib.h /mingw/include
# cp -iv libz.a libz.dll.a /mingw/lib
then
	export PATH=/d/QtSDK/Desktop/Qt/4.8.1/mingw/bin:$PATH
	#qmake -o msrtoolMakeFile msrtool.pro
	qmake -o msrtoolMakeFile msrtool.pro -r -spec win32-g++
	mingw32-make -f msrtoolMakeFile.Release > /dev/null
fi

rm -rf msrtoolMakeFile
#cp msrtool installations
